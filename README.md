# How to run application
> 
> Application uses newest version of *ReactRouter* - node verion 16+ is required to run application!!!

## Step 1
Go to the root directory of the project and install required packages using command:
```
npm i
```
## Step 2
Start application typing command:
```
npm run start
```
Application will be running on port 3000