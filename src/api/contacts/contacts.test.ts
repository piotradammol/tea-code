// import axios from 'axios';
import { getContacts } from './contacts.client';

// jest.mock("axios");
// const mockedAxios = axios as jest.Mocked<typeof axios>;

const mockedContacts = [
  {
    id: 1,
    first_name: 'Suzie',
    last_name: 'Kydd',
    email: 'skydd0@prnewswire.com',
    gender: 'Female',
    avatar: 'https://robohash.org/fugiatautemodit.png?size=50x50&set=set1',
  },
  { id: 2, first_name: 'Finley', last_name: 'Fenich', email: 'ffenich1@spotify.com', gender: 'Male', avatar: null },
];

const mockedResponse = {
  json() {
    return mockedContacts;
  },
};

describe('Contacts api unit tests', () => {
  let fakeFetch: jest.Mock;

  beforeAll(() => {
    fakeFetch = jest.fn();
    window.fetch = fakeFetch;
  });

  afterAll(() => {
    jest.clearAllMocks();
  });

  test('Returns list of contacts', async () => {
    fakeFetch.mockResolvedValue(mockedResponse);

    const contacts = await getContacts();

    expect(contacts.length).toEqual(2);
    expect(contacts[0].id).toEqual(mockedContacts[0].id);
    expect(contacts[0].first_name).toEqual(mockedContacts[0].first_name);
    expect(contacts[0].last_name).toEqual(mockedContacts[0].last_name);
    expect(contacts[0].email).toEqual(mockedContacts[0].email);
    expect(contacts[0].gender).toEqual(mockedContacts[0].gender);
    expect(contacts[0].avatar).toEqual(mockedContacts[0].avatar);
  });

  test('Returns empty array when no contacts returned', async () => {
    fakeFetch.mockResolvedValue({
      json() {
        return [];
      },
    });

    const contacts = await getContacts();

    expect(contacts).toEqual([]);
  });

  test('Returns empty array when some error occurred', async () => {
    fakeFetch.mockRejectedValue('Some error');

    const contacts = await getContacts();

    expect(contacts).toEqual([]);
  });
});
