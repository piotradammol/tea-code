export enum Gender {
  female = 'Female',
  male = 'Male',
}

export interface Contact {
  id: number;
  first_name: string;
  last_name: string;
  email: string;
  gender: Gender;
  avatar?: string;
}
