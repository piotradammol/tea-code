import { Contact } from './contact.dto';

const API_URL = 'https://teacode-recruitment-challenge.s3.eu-central-1.amazonaws.com/users.json';

export const getContacts = async (): Promise<Contact[]> => {
  try {
    const response = await fetch(API_URL, {
      headers: new Headers({ 'content-type': 'application/json' }),
    });
    const contactsDto = (await response.json()) as Contact[];
    return contactsDto;
  } catch (err) {
    return [];
  }
};
