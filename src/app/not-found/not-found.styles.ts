import styled from 'styled-components';

export const NotFoundContainer = styled.div`
  display: flex;
  width: 100vw;
  height: 100vh;
  align-items: center;
  justify-content: center;
`;

export const NotFoundText = styled.p`
  size: 24px;
`;
