import { NotFoundContainer, NotFoundText } from './not-found.styles';

export const PageNotFound = () => {
  return (
    <NotFoundContainer>
      <NotFoundText> Page not found </NotFoundText>
    </NotFoundContainer>
  );
};
