import { FC, useCallback, useEffect, useState } from 'react';

import { Contact } from '../../api/contacts/contact.dto';
import { getContacts } from '../../api/contacts/contacts.client';
import { TableRow } from '../common/table-row/table-row';

import { ContactsContainer } from './contacts.styles';

export const Contacts: FC = () => {
  const [contacts, setContacts] = useState<Contact[]>([]);
  const [rows, setRows] = useState<JSX.Element[]>([]);
  const [checkedRows, setCheckedRows] = useState<number[]>([]);

  const fetchContacts = useCallback(async () => {
    const newContats = await getContacts();
    const sortedContacts = newContats.sort((contactA, contactB) => (contactA.last_name > contactB.last_name ? 1 : -1));
    setContacts(sortedContacts);
  }, []);

  const onRowClick = (id: number, isChecked: boolean) => {
    if (isChecked) {
      setCheckedRows([...checkedRows, id]);
    } else {
      setCheckedRows(checkedRows.filter((key) => key !== id));
    }
  };

  const getRows = () => {
    return contacts.map((contact) => {
      return (
        <TableRow
          id={contact.id}
          title={`${contact.first_name} ${contact.last_name}`}
          subtitle={contact.email}
          thumbnail={contact.avatar}
          onClickAction={onRowClick}
        />
      );
    });
  };

  useEffect(() => {
    fetchContacts();
  }, []);

  useEffect(() => {
    setRows(getRows());
  }, [contacts, checkedRows]);

  useEffect(() => {
    /* eslint-disable no-console */
    console.log(checkedRows);
  }, [checkedRows]);

  return <ContactsContainer>{rows}</ContactsContainer>;
};
