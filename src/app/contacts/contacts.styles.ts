import styled from 'styled-components';

export const ContactsContainer = styled.div`
  width: 100%;
  padding: 24px;
  box-sizing: border-box;
`;
