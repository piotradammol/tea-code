/* eslint-disable import/no-default-export */
import React from 'react';
import { BrowserRouter } from 'react-router-dom';

import { AppRoutes } from '../routing/app-routes';

import { Navbar } from './common/navbar/navbar';

function App() {
  return (
    <BrowserRouter>
      <Navbar />
      <AppRoutes />
    </BrowserRouter>
  );
}

export default App;
