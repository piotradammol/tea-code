import { Link } from 'react-router-dom';

import { AppRoute } from '../../../routing/app-route.enum';

import { NavbarContainer, NavbarItem } from './navbar.styles';

export const Navbar = () => {
  return (
    <NavbarContainer>
      <NavbarItem>
        <Link to={AppRoute.contacts}>Contacts</Link>
      </NavbarItem>
    </NavbarContainer>
  );
};
