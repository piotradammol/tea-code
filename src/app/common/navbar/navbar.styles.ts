import styled from 'styled-components';

export const NavbarContainer = styled.nav`
  width: 100%;
  height: 64px;
  display: flex;
  justify-content: center;
  align-items: center;
  background: linear-gradient(90deg, rgba(2, 0, 36, 1) 0%, rgba(9, 9, 121, 1) 35%, rgba(0, 212, 255, 1) 100%);
`;

export const NavbarItem = styled.div`
  color: white;
  size: 24px;
  padding: 12px;
  cursor: pointer;
  & > a {
    text-decoration: none !important;
    color: inherit;
  }
`;
