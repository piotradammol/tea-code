import React, { useMemo, useState } from 'react';
import { FC } from 'react';

import {
  Checkbox,
  DefaultThumbnail,
  DefaultThumbnailText,
  Subtitle,
  TableRowContainer,
  TextsContainer,
  Thumbnail,
  Title,
} from './table-row.styles';

export type TableRowOnClickAction = (id: number, isChecked: boolean) => void;

export interface TableRowProperties {
  id: number;
  title: string;
  subtitle: string;
  thumbnail?: string;
  onClickAction?: TableRowOnClickAction;
}

export const TableRow: FC<TableRowProperties> = ({ id, title, subtitle, thumbnail, onClickAction }) => {
  const [isChecked, setIsChecked] = useState(false);

  const thumbnailElement = useMemo(() => {
    const words = title.split(' ');
    const abbriviation = words
      .map((w) => {
        return w.charAt(0).toUpperCase();
      })
      .join('');

    if (thumbnail) {
      return <Thumbnail src={thumbnail} />;
    }
    return (
      <DefaultThumbnail>
        <DefaultThumbnailText>{abbriviation}</DefaultThumbnailText>
      </DefaultThumbnail>
    );
  }, [thumbnail, title]);

  const toggleCheckBox = () => {
    setIsChecked(!isChecked);
    if (onClickAction) onClickAction(id, !isChecked);
  };

  return (
    <TableRowContainer
      onClick={() => {
        toggleCheckBox();
      }}
      key={id}
    >
      {thumbnailElement}
      <TextsContainer>
        <Title>{title}</Title>
        <Subtitle>{subtitle}</Subtitle>
      </TextsContainer>
      <Checkbox checked={isChecked} readOnly={true} />
    </TableRowContainer>
  );
};
