import styled from 'styled-components';

export const TableRowContainer = styled.div`
  width: 100%;
  padding: 8px;
  border-bottom: 1px solid var(--light-grey);
  display: flex;
  align-items: center;
  cursor: pointer;
`;
export const Thumbnail = styled.img`
  border-radius: 50%;
  height: 64px;
  border: 1px solid var(--light-grey);
`;

export const DefaultThumbnail = styled.div`
  height: 64px;
  width: 64px;
  border-radius: 50%;
  background-color: var(--default-white);
  border: 1px solid var(--light-grey);
  display: flex;
  align-items: center;
  justify-content: center;
`;

export const DefaultThumbnailText = styled.div`
  color: var(--light-grey);
  size: 24px;
`;

export const TextsContainer = styled.div`
  display: flex;
  flex-direction: column;
  padding: 16px;
`;

export const Title = styled.div`
  size: 16px;
  color: var(--black);
`;

export const Subtitle = styled.div`
  size: 12px;
  color: var(--dark-grey);
`;

export const Checkbox = styled.input.attrs({ type: 'checkbox' })`
  width: 24px;
  height: 24px;
  margin-left: auto;
`;
