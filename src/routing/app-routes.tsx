import React from 'react';
import { Route, Routes } from 'react-router-dom';

import { Contacts } from '../app/contacts/contacts';
import { PageNotFound } from '../app/not-found/not-found';

import { AppRoute } from './app-route.enum';

export const AppRoutes = () => {
  return (
    <Routes>
      <Route path={AppRoute.contacts} element={<Contacts />} />
      <Route path="*" element={<PageNotFound />} />
    </Routes>
  );
};
